from django.shortcuts import render
from .common.encoders import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import User, Reward, Notification
from django.conf import settings
import djwto.authentication as auth
import json
import os
import pika

class RewardEncoder(ModelEncoder):
    model = Reward
    properties = [
        "pk",
        "name",
        "picture",
        "required_points"
    ]

# Json encoder imported from the encoder file in the common folder(more information on the encoder in encoder file)
class UserEncoder(ModelEncoder):
    model = User
    properties = [
        "username",
        "first_name",
        "last_name",
        "spendable_points",
        "lifetime_points",
        "notifications_on",
        "is_active",
        "id",
        "rewards"
    ]
    encoders = {
        "rewards": RewardEncoder()
    }

class NotificationEncoder(ModelEncoder):
    model = Notification
    properties = [
        "message",
        "read",
        "id"
    ]

# this function sends the user info to other microservices through rabbitmq
def send_user_data(user):
    url = os.environ.get('CLOUDAMQP_URL_TASK', "amqp://guest:guest@rabbitmq")
    parameters = pika.URLParameters(url)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    # used a fanout exchange type because as the app grows there may be multiple microservices that require a UserVo model
    channel.exchange_declare(exchange="user_info", exchange_type="fanout")

    message = json.dumps(user, cls=UserEncoder)
    channel.basic_publish(
        exchange="user_info",
        routing_key="",
        body=message,
    )
    connection.close()



# helper function to determine if username is in use
def username_in_use(username):
    user = User.objects.filter(username=username).count()
    if user > 0:
        return True
    return False


# view function used to get a list of all users or create a new user
@require_http_methods(["GET", "POST"])
def user_list(request):
    if request.method == "GET":
        users = User.objects.all()
        return JsonResponse(
            {"Users": users},
            encoder=UserEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            user = User.objects.create(**content)
            send_user_data(user)
            return JsonResponse(
                {"User": user},
                encoder=UserEncoder
            )
        except:
            # uses helper function to check if reason update failed was due to existing user with new username
            potential_username = content["username"]
            username_exists = username_in_use(potential_username)
            if username_exists:
                return JsonResponse(
                    {"Error": "Username is already in use, please choose a new username."}
                )
            response = JsonResponse({"Error": "User was not able to be created"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def active_user_list(request):
    users = User.objects.filter(is_active=True)
    return JsonResponse(
        {"Users": users},
        encoder=UserEncoder
    )


@require_http_methods(["GET", "PUT", "DELETE"])
def individual_user(request, username):
    if request.method == "GET":
        try:
            user = User.objects.get(username=username)
            return JsonResponse(
                {"User": user},
                encoder=UserEncoder
            )
        except User.DoesNotExist:
            response = JsonResponse({"Error": "Was not able to find user"})
            response.status_code = 400
            return response
    elif request.method == "PUT":
        content = json.load(request.body)

        try:
            user = User.objects.get(username=username).update(**content)
            return JsonResponse(
                {"User": user},
                encoder=UserEncoder
            )
        except:
            content = json.loads(request.body)
            # uses helper function to check if reason update failed was due to existing user with new username
            potential_username = content["username"]
            # need to check if username has been changed because if it has not been username_in_use will allows 
            # return True and say that it is the username's fault when it may not be
            if potential_username != username:
                username_exists = username_in_use(potential_username)
                if username_exists:
                    return JsonResponse(
                        {"Error": "Username is already in use, please choose a new username."}
                    )
            return JsonResponse(
                {"Error": "Was not able to update user please make sure all fields are filled in."}
            )
    else:
        try:
            user = User.objects.get(username=username)
            # keeps user data in the system and just changes is_active to False
            user.is_active = False
            user.save()
            return JsonResponse(
                {"User": user},
                encoder=UserEncoder
            )
        except:
            return JsonResponse({"Error": "Was unable to delete user"})


# this view gets the current logged in user's username in order to be used on the front end to access the GET user detail function via username
# from a jwt_access_token 
# jwt_login_required protects this view from being accessed by unauthorized user, it also allows the payload to be included, this is a combination of
# setting the fetch configuration's credentials to include on the front end and the auth.jwt_login_required which allows 
@auth.jwt_login_required
@require_http_methods("GET")
def get_payload_token(request):
    # automatically decodes the jwt_access_token
    token_data = request.payload
    username = token_data["user"]
    if username:
        return JsonResponse({"username": username})
    response = JsonResponse({"username": None})
    return response

@require_http_methods(["GET"])
def get_leader_board(request):
    # selects the ten top users by lifetime_points property
    users = User.objects.all().order_by("-lifetime_points")[:10]
    url = os.environ.get('CLOUDAMQP_URL_TASK', "amqp://guest:guest@rabbitmq")

    return JsonResponse(
        {"Users": users, "url": url},
        encoder=UserEncoder
    )

# returns all rewards
@require_http_methods(["GET"])
def reward_list(request):
    rewards = Reward.objects.all()

    return JsonResponse(
        {"Rewards": rewards},
        encoder=RewardEncoder
    )

def check_user_spendable_points(user, cost):
    if user.spendable_points >= cost:
        return True
    else: 
        return False

# @auth.jwt_login_required
@require_http_methods(["PUT"])
def purchase_reward(request, reward_pk, user_id):
    content = json.loads(request.body)
    try:
        cost = content["Cost"]
        reward = Reward.objects.get(pk=reward_pk)
        
        # Django doesn't allow .update method on when getting the user by .get only filter
        # so I need to create two variables one a queryset that I can use the .update method on
        # and on which will be at index 0 which is the user instance that I need to add the 
        # reward to the user
        update_user = User.objects.filter(id=user_id)
        user = update_user[0]

        if check_user_spendable_points(user, cost):
            new_spendable_points = user.spendable_points - cost
            user.rewards.add(reward)
            update_user.update(spendable_points=new_spendable_points) 

        else:
            return JsonResponse(
                {"Cost": "You do not have enough spendable points"}
            )

    except:
        return JsonResponse(
            {"Error": "Was unable to purchase reward"}
        )

    return JsonResponse(
        {"User": user},
        encoder=UserEncoder
    )


@require_http_methods(["GET"])
def notification_list(request):
    notifications = Notification.objects.all()

    return JsonResponse(
        {"Notifications": notifications},
        encoder=NotificationEncoder
    )

@require_http_methods(["GET"])
def user_unread_notifications(request, user_id):
    notifications = Notification.objects.filter(user__id=user_id, read=False)
    
    return JsonResponse(
        {"Notifications": notifications},
        encoder=NotificationEncoder
    )

@require_http_methods(["GET"])
def number_of_unread_user_notifications(request, user_id):
    notifications = Notification.objects.filter(user__id=user_id, read=False)
    notifications_count = notifications.count()

    return JsonResponse(
        {"Count": notifications_count}
    )

@require_http_methods(["PUT"])
def mark_notification_read(request, notification_id):
    notification = Notification.objects.filter(id=notification_id)

    notification.update(read=True)

    return JsonResponse(
        {"Notification": notification},
        encoder=NotificationEncoder
    )