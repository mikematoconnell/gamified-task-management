from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class Reward(models.Model):
    name = models.CharField(max_length=20)
    picture = models.URLField()
    required_points = models.BigIntegerField(default=100)

#used Django's AbstractUser class to extend the functionality of the user class with its default fields
class User(AbstractUser):
    first_name = models.CharField(max_length=300)
    last_name = models.CharField(max_length=300)
    username = models.CharField(max_length=10, unique=True)
    lifetime_points = models.BigIntegerField(default=0)
    spendable_points = models.BigIntegerField(default=0)
    notifications_on = models.BooleanField(default=True)
    date_joined = models.DateField(auto_now_add=True, null=True)
    rewards = models.ManyToManyField(
        Reward,
        blank=True,
        related_name="reward"
    )

    def __str__(self):
        return f"Username: {self.username}, First Name: {self.first_name}, Last Name: {self.last_name}"

    # save function makes sure that if created user is not a superuser(is_staff) that the password will appropriately be hashed
    # createsuper user does this by default and if not specified it will double hash super user passwords
    def save(self, *args, **kwargs):
        if self.is_staff is not True:
            self.set_password(self.password)
            super().save(*args, **kwargs)
        else:
            super().save(*args, **kwargs)


class Notification(models.Model):
    message = models.CharField(max_length=200)
    # will use the read property to filter which one are still displayed for the user
    read = models.BooleanField(default=False)
    created_date = models.DateField(auto_now=True)
    # this property is used just for when a notification is created for a due date otherwise the poller would continually create new instances of it
    # would have preferred to use a foreignkey to a taskvo that would be created by a poller and a foreign key to a task_type model but I am running out of time
    due_date_task_id = models.PositiveBigIntegerField(default=None, blank=True, null=True)
    user = models.ForeignKey(
        User,
        related_name="notification",
        on_delete=models.CASCADE,
    )