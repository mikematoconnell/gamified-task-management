from django.urls import path

from .views import (
    user_list,
    individual_user,
    active_user_list,
    get_payload_token,
    get_leader_board,
    reward_list,
    purchase_reward,
    notification_list,
    user_unread_notifications,
    number_of_unread_user_notifications,
    mark_notification_read,
)


urlpatterns = [
    path("", user_list, name="list_all_users"),
    path("individual_user/<str:username>/", individual_user, name="individual_user"),
    path("active_users/", active_user_list, name="active_users"),
    # path("token/mine", api_user_token, name="get_my_token"),
    path("token/", get_payload_token, name="get_payload_token"),
    path("leader_board/", get_leader_board, name="get_leader_board"),
    path("reward_list/", reward_list, name="reward_list"),
    path("purchase_reward/<int:user_id>/<int:reward_pk>/", purchase_reward, name="purchase_reward"),
    path("notifications/all/", notification_list, name="notification_list"),
    path("notifications/user_unread/<int:user_id>/", user_unread_notifications, name="user_unread_notifications"),
    path("notifications/count/<int:user_id>/", number_of_unread_user_notifications, name="number_of_unread_user_notifications"),
    path("notifications/mark_complete/<int:notification_id>/", mark_notification_read, name="mark_notification_read")
]  