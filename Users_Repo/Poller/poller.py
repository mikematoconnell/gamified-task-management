import django
import os
import sys
import time
import json
import requests
from datetime import date


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Users_Project.settings")
django.setup()

from Users_App.models import User, Notification
TASK_API = os.environ["TASKS_API"]

url = f"{TASK_API}/tasks/task_list/"


def create_due_date_notification(user, title, task_id, today):
    message = f"Your task {title} is due today ({today})."
    Notification.objects.create(user=user, message=message, due_date_task_id=task_id)

def check_if_message_already_created(task_id):
    matching_notifications = Notification.objects.filter(due_date_task_id=task_id)
    
    if matching_notifications.count() > 0:
        return False
    return True


def check_for_tasks_due_today():
    response = requests.get(url)
    content = json.loads(response.content) 
    tasks = content["Tasks"]

    today = str(date.today())

    for task in tasks:
        if task["due_date"] == today and task["status"]["status_type"] != "Completed":
            user_id = task["user"]["uservo_id"]
            user = User.objects.get(id=user_id)
            title = task["title"]
            task_id = task["id"]
    
            if check_if_message_already_created(task_id):
                create_due_date_notification(user, title, task_id, today)

       

def poll():
    while True:
        try:
            check_for_tasks_due_today()
        except Exception as e:
            print(e, file=sys.stderr)
            print("Due Date poller failed")
        time.sleep(10)


if __name__ == "__main__":
    poll()
