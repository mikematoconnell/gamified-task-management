import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Users_Project.settings")
django.setup()

from Users_App.models import User, Notification


# this function updates the user's spendable_points and lifetime_points properties
def update_User_points(user, completion_points):
    current_spendable_points = user.spendable_points
    current_lifetime_points = user.lifetime_points

    #calculates the new totals that will be updated on the user model
    total_spendable_points = completion_points + current_spendable_points   
    total_lifetime_points = completion_points + current_lifetime_points

    # seems that only filter operations rather than also get allow for the update operation  
    User.objects.filter(id=user.id).update(spendable_points=total_spendable_points, lifetime_points=total_lifetime_points)

#this function creates a notification instance that relates to the user who completed the task letting them know how many points they were awarded
def create_completion_notification(user, title, completion_points):
    message = f"Congratulations on complete your task {title}! You have been awarded {completion_points} points. Keep you the good work."
    Notification.objects.create(user=user, message=message)

# get data out of the body of the rabbitmq function from tasks microservice and calls the two above functions
def update_User_and_create_notification(ch, method, properties, body):
    content = json.loads(body)
    user_id = content["id"]
    completion_points = content["points"]
    title = content["title"]
    user = User.objects.get(id=user_id)
    
    update_User_points(user, completion_points)
    create_completion_notification(user, title, completion_points)










def consume():
    while True:
        try:
            url = os.environ.get("CLOUDAMQP_URL", "amqp://guest:guest@rabbitmq")
            parameters = pika.URLParameters(url)
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            channel.exchange_declare(exchange="task_complete_info", exchange_type="fanout")
            result = channel.queue_declare(queue='', exclusive=True)
            queue_name = result.method.queue
            channel.queue_bind(exchange='task_complete_info', queue=queue_name)
            channel.basic_consume(
                queue=queue_name,
                on_message_callback=update_User_and_create_notification,
                auto_ack=True,
            )
            channel.start_consuming()
        except AMQPConnectionError:
            print("It could not connect to RabbitMQ")
            time.sleep(3.0)

if __name__ == "__main__":
    consume()