# Gamified Task Management


## Getting started

Hosted at: https://mikematoconnell.gitlab.io/gamified-task-management/
Username: Test
Password: test1234

To run locally:

- clone repository
- make sure you have docker desktop installed and running
- in the top level of the project run the following commands
- docker volume create task-application
- docker-compose build
- docker-compose up

Once all containers are running you will be able to view the application at http://localhost:3000/ in your browser.


Tests are set up to run on deployment, but are unable to run locally due to Heroku's DATABASE_URL setup.