import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Tasks_Project.settings")
django.setup()

from Tasks_App.models import UserVO


def update_UserVO(ch, method, properties, body):
    content = json.loads(body)
    user_id = content["id"]

    UserVO.objects.update_or_create(
        uservo_id=user_id,
        defaults={
            "uservo_id": user_id
        }
    )


def consume():
    while True:
        try:
            url = os.environ.get("CLOUDAMQP_URL", "amqp://guest:guest@rabbitmq")
            parameters = pika.URLParameters(url)
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            channel.exchange_declare(exchange="user_info", exchange_type="fanout")
            result = channel.queue_declare(queue='', exclusive=True)
            queue_name = result.method.queue
            channel.queue_bind(exchange='user_info', queue=queue_name)
            channel.basic_consume(
                queue=queue_name,
                on_message_callback=update_UserVO,
                auto_ack=True,
            )
            channel.start_consuming()
        except AMQPConnectionError:
            print("It could not connect to RabbitMQ")
            time.sleep(3.0)

if __name__ == "__main__":
    consume()