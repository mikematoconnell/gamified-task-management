from django.urls import path
from .views import (
    user_vo_list,
    task_list,
    status_list,
    priority_list,
    task_detail,
    all_users_tasks,
    upcoming_user_tasks,
    search_user_tasks,
    due_date_user_tasks,
    priority_user_tasks,
    status_user_tasks,
    upcoming_search_user_tasks,
    upcoming_priority_user_tasks,
    upcoming_status_user_tasks,
    upcoming_status_priority_user_tasks,
    upcoming_search_priority_user_tasks,
    upcoming_search_status_user_tasks,
    upcoming_search_status_priority_user_tasks,
    search_due_date_user_tasks,
    search_priority_user_tasks,
    search_status_user_tasks,
    search_status_priority_user_tasks,
    search_due_date_priority_user_tasks,
    search_due_date_status_user_tasks,
    search_due_date_priority_status_user_tasks,
    due_date_status_user_tasks,
    due_date_priority_user_tasks,
    due_date_priority_status_user_tasks,
    priority_status_user_tasks,
    completed_date_user_tasks,
    completed_date_priority_user_tasks,
    completed_date_search_user_tasks,
    completed_date_priority_status_user_tasks,
    completed_date_priority_search_user_tasks,
    completed_date_status_search_user_tasks,
    completed_date_status_priority_search_user_tasks,
    mark_task_complete,
    update_task,
    delete_task
)

# the path's url name may be a bit long the the reason I did this was if latter their would be a general task search for instance regardless of user
# then it could be differentiated
urlpatterns = [
    path("uservo_list/", user_vo_list, name="uservo_list"),
    path("task_list/", task_list, name="task_list"),
    path("status_list/", status_list, name="status_list"),
    path("priority_list/", priority_list, name="priority_list"),
    path("task_detail/<int:task_id>/", task_detail, name="task_detail"),
    
    

    # seperate for easier reading
    # get requests cannot have bodies so need to include variable in url
    # desired_order and user_id include first so on front end can create a longer baseUrl string with less typing
    
    # all user's tasks
    path("task_list/user/<str:desired_order>/<int:user_id>/all_tasks/", all_users_tasks, name="all_users_tasks"),
    # all upcoming user's tasks
    path("task_list/user/<str:desired_order>/<int:user_id>/upcoming_tasks/", upcoming_user_tasks, name="upcoming_user_tasks"),
    # all user's tasks title or description match search_phrase  
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/search_tasks/", search_user_tasks, name="search_user_tasks"),
    # all user's tasks where duedate matches
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:due_date>/due_date_tasks/", due_date_user_tasks, name="date_user_tasks"),
    # all user's tasks where priority matches one of the given priorities
    path("task_list/user/<str:desired_order>/<int:user_id>/<int:priority1>/<int:priority2>/<int:priority3>/priority_tasks/", priority_user_tasks, name="priority_user_tasks"),
    # all user's tasks where status matches one of the given statuses
    path("task_list/user/<str:desired_order>/<int:user_id>/<int:status1>/<int:status2>/<int:status3>/status_tasks/", status_user_tasks, name="status_user_tasks"),
    # all upcoming user's tasks where search_phrase matches title or description
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/upcoming_search_tasks/", upcoming_search_user_tasks, name="upcoming_search_user_tasks"),
    # all upcoming user's tasks where priority matches one of the given priorities
    path("task_list/user/<str:desired_order>/<int:user_id>/<int:priority1>/<int:priority2>/<int:priority3>/upcoming_priority_tasks/", upcoming_priority_user_tasks, name="upcoming_priority_user_tasks"),
    # all upcoming user's tasks where status matches one of the given statuses
    path("task_list/user/<str:desired_order>/<int:user_id>/<int:status1>/<int:status2>/<int:status3>/upcoming_status_tasks/", upcoming_status_user_tasks, name="upcoming_status_user_tasks"),
    # upcoming status priority
    path("task_list/user/<str:desired_order>/<int:user_id>/<int:status1>/<int:status2>/<int:status3>/<int:priority1>/<int:priority2>/<int:priority3>/upcoming_status_priority_tasks/", upcoming_status_priority_user_tasks, name="upcoming_status_priority_user_tasks"),
    #upcoming search priority
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/<int:priority1>/<int:priority2>/<int:priority3>/upcoming_search_priority_tasks/", upcoming_search_priority_user_tasks, name="upcoming_search_priority_user_tasks"),
    # upcoming search status
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/<int:status1>/<int:status2>/<int:status3>/upcoming_search_status_tasks/", upcoming_search_status_user_tasks, name="upcoming_search_status_user_tasks"),
    # upcoming search priority status
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/<int:status1>/<int:status2>/<int:status3>/<int:priority1>/<int:priority2>/<int:priority3>/upcoming_search_status_priority_tasks/", upcoming_search_status_priority_user_tasks, name="upcoming_search_status_priority_user_tasks"),
    # search due_date
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/<str:due_date>/search_due_date_tasks/", search_due_date_user_tasks, name="search_due_date_user_tasks"),
    # search priority
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/<int:priority1>/<int:priority2>/<int:priority3>/search_priority_tasks/", search_priority_user_tasks, name="search_priority_user_tasks"),
    # search status
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/<int:status1>/<int:status2>/<int:status3>/search_status_tasks/", search_status_user_tasks, name="search_status_user_tasks"),
    #search status priority
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/<int:status1>/<int:status2>/<int:status3>/<int:priority1>/<int:priority2>/<int:priority3>/search_status_priority_tasks/", search_status_priority_user_tasks, name="search_status_priority_tasks"),  
    # search due_date priority
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/<int:priority1>/<int:priority2>/<int:priority3>/<str:due_date>/search_due_date_priority_tasks/", search_due_date_priority_user_tasks, name="search_due_date_priority_user_tasks"),
    # search due_date status
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/<int:status1>/<int:status2>/<int:status3>/<str:due_date>/search_due_date_status_tasks/", search_due_date_status_user_tasks, name="search_due_date_status_user_tasks"),
    # search due_date status priority
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/<int:status1>/<int:status2>/<int:status3>/<int:priority1>/<int:priority2>/<int:priority3>/<str:due_date>/search_due_date_priority_status_tasks/", search_due_date_priority_status_user_tasks, name="search_due_date_priority_status_user_tasks"),
    # due_date status
    path("task_list/user/<str:desired_order>/<int:user_id>/<int:status1>/<int:status2>/<int:status3>/<str:due_date>/due_date_status_tasks/", due_date_status_user_tasks, name="due_date_status_user_tasks"),
    # due_date priority
    path("task_list/user/<str:desired_order>/<int:user_id>/<int:priority1>/<int:priority2>/<int:priority3>/<str:due_date>/due_date_priority_tasks/", due_date_priority_user_tasks, name="due_date_priority_user_tasks"),
    # due_date status priority
    path("task_list/user/<str:desired_order>/<int:user_id>/<int:status1>/<int:status2>/<int:status3>/<int:priority1>/<int:priority2>/<int:priority3>/<str:due_date>/due_date_priority_status_tasks/", due_date_priority_status_user_tasks, name="due_date_priority_status_user_tasks"),
    # priority status
    path("task_list/user/<str:desired_order>/<int:user_id>/<int:status1>/<int:status2>/<int:status3>/<int:priority1>/<int:priority2>/<int:priority3>/priority_status_tasks/", priority_status_user_tasks, name="priority_status_user_tasks"),
    # completed_date
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:completed_date>/completed_tasks/", completed_date_user_tasks, name="completed_date_user_tasks"),
    # completed_date priority
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:priority1>/<str:priority2>/<int:priority3>/<str:completed_date>/completed_priority_tasks/", completed_date_priority_user_tasks, name="completed_date_priority_user_tasks"),
    # completed search
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/<str:completed_date>/completed_search_tasks/", completed_date_search_user_tasks, name=" completed_date_search_user_tasks"),
    # completed_date status priority
    path("task_list/user/<str:desired_order>/<int:user_id>/<int:status1>/<int:status2>/<int:status3>/<int:priority1>/<int:priority2>/<int:priority3>/<str:completed_date>/completed_status_priority_tasks/", completed_date_priority_status_user_tasks, name="completed_date_priority_status"),
    # completed_date priority search
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/<int:priority1>/<int:priority2>/<int:priority3>/<str:completed_date>/completed_priority_search_tasks/", completed_date_priority_search_user_tasks, name="completed_date_priority"),
    # completed_date status search
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/<int:status1>/<int:status2>/<int:status3>/<str:completed_date>/completed_status_search_tasks/", completed_date_status_search_user_tasks, name="completed_date_status_search_user_tasks"),
    # completed_date status priority search
    path("task_list/user/<str:desired_order>/<int:user_id>/<str:search_phrase>/<int:status1>/<int:status2>/<int:status3>/<int:priority1>/<int:priority2>/<int:priority3>/<str:completed_date>/completed_status_priority_search_tasks/", completed_date_status_priority_search_user_tasks, name="completed_date_status_priority_search_user_tasks"),

    path("mark_complete/<int:task_id>/", mark_task_complete, name="mark_task_complete"),

    path("update_task/<int:task_id>/", update_task, name="update_task"),

    path("delete_task/<int:task_id>/", delete_task, name="delete_task"),
] 
