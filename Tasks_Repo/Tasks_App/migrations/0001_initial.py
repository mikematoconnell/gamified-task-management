# Generated by Django 4.0.3 on 2023-02-23 03:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Priority',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('priority_type', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status_type', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='UserVO',
            fields=[
                ('uservo_id', models.PositiveBigIntegerField(primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=20)),
                ('description', models.TextField()),
                ('due_date', models.DateField()),
                ('completed_date', models.DateField()),
                ('priority', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='task', to='Tasks_App.priority')),
                ('status', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='task', to='Tasks_App.status')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='task', to='Tasks_App.uservo')),
            ],
        ),
    ]
