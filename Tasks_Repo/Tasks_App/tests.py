from django.test import TestCase
from django.urls import reverse
import json
from .models import UserVO, Status, Priority, Task

# Create your tests here.

class TestTaskView(TestCase):
    def setUp(self):
        self.UserVO = UserVO.objects.create(
            uservo_id=1
        )

        self.Status = Status.objects.create(
            id=1,
            status_type="To-do"
        )

        self.Priority = Priority.objects.create(
            id=1,
            priority_type="Urgent"
        )


        self.Task = Task.objects.create(
            id=1,
            title="Walk Tiger",
            description="A nice walk through the forest with the cat",
            due_date="2023-05-05",
            user=self.UserVO,
            status=self.Status,
            priority=self.Priority
        )

    def test_task_list(self):
        response = self.client.get("/tasks/task_detail/1/")
        content = response.json()
        self.assertEqual(response.status_code, 200)
        # checks if the response gives back a success status code
        task = content["Task"]
        self.assertEqual(task["title"], "Walk Tiger")