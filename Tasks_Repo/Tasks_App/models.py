from django.db import models

# Create your models here.
# will be used as a value object to link users to their tasks, will only use user_id because 
# this field will not be updated by the user, therefore will be immutable
class UserVO(models.Model):
    uservo_id = models.PositiveBigIntegerField(primary_key=True)


# create a model that will represent 3 given status types (to-do, in-progress, completed)
# which will be created using fixtures and linked to tasks through a foreign key
class Status(models.Model):
    status_type = models.CharField(max_length=20)


# used similarly to the status class, 3 types (urgent, important, unimportant) will 
# be created with fixtures and linked to tasks through a foreign key
class Priority(models.Model):
    priority_type = models.CharField(max_length=20)


class Task(models.Model):
    title = models.CharField(max_length=100, null=False)
    description = models.TextField()
    due_date = models.DateField()
    completed_date = models.DateField(null=True)
    # included a deleted field so users can delete their tasks and they will not show up for them but
    # the data is preserved
    deleted = models.BooleanField(default=False)
    user = models.ForeignKey(
        UserVO,
        related_name="task",
        on_delete=models.CASCADE,
        null=False,
    )
    status = models.ForeignKey(
        Status,
        related_name="task",
        on_delete=models.PROTECT,
        null=False,
    )
    priority = models.ForeignKey(
        Priority,
        related_name="task",
        on_delete=models.PROTECT,
        null=False,
    )

    

