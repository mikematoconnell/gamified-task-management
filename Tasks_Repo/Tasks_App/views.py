from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.db.models import Q
from datetime import date
from .common.encoders import ModelEncoder
from .models import UserVO, Status, Priority, Task
import json
import pika
import os

# Create your views here.

# Encoders to convert data into Json acceptable form imported from the encoder.py file in common
class UserVOEncoder(ModelEncoder):
    model = UserVO
    properties = [
        "uservo_id"
    ]

class StatusEncoder(ModelEncoder):
    model = Status
    properties = [
        "status_type",
        "id"
    ]


class PriorityEncoder(ModelEncoder):
    model = Priority
    properties = [
        "priority_type",
        "id"
    ]


class TaskEncoder(ModelEncoder):
    model = Task
    properties = [
        "id",
        "title",
        "description",
        "due_date",
        "completed_date",
        "user",
        "status",
        "priority",
        "deleted"
    ]
    encoders = {
        "status": StatusEncoder(),
        "priority": PriorityEncoder(),
        "user": UserVOEncoder()
    }

# filters by user returning tasks matching the user's id to the user_id variable passed through
def filter_by_user(queryset, user_id):
    tasks = queryset.filter(user=user_id)
    return tasks

# claculates points 
def calculate_task_points(user_id):
    # base amount for completed task is 10 points
    points = 10
    today = date.today()
    tasks = Task.objects.all()
    
    # get user tasks to check due_date and completed_date to calculate score
    user_tasks = filter_by_user(tasks, user_id)

    # check if there are any tasks that are still due today
    # if no tasks are due today the user gets an additional 15 points
    tasks_due_today = user_tasks.filter(due_date=today)
    if tasks_due_today.exists():
        pass
    else:
         points += 15
    
    # if more than 3 tasks have been completed the user gets an additional 20 points counting the just completed task
    tasks_completed_today = user_tasks.filter(completed_date=today)
    completed_count = tasks_completed_today.count()
    if completed_count > 3:
        points += 20
    
    return points


# rabbitmq function to update user points in the user microservice
def send_points_data(user_id, title):
    # calculates the points that will be sent
    points = calculate_task_points(user_id)
    url = os.environ.get("CLOUDAMQP_URL_USER", "amqp://guest:guest@rabbitmq")

    parameters = pika.URLParameters(url)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    # used fanout in case there are microservices built in the future that need this information
    channel.exchange_declare(exchange="task_complete_info", exchange_type="fanout")
    
    # need to include user id and the points that they have been awarded
    message = json.dumps({"id": user_id, "points": points, "title": title})   
    channel.basic_publish(
        exchange="task_complete_info",
        routing_key="",
        body=message,
    )
    connection.close()

# helper functions created to reduce code reuse
# Reason behind creating these helper functions is it makes the code easier to read and easier to refactor

# gets data from the content
def pull_data_content(content):
    # considered using itemgetter here but the length was too long and it didn't allow for empty parameters
    # square brackets are used for user and desired order because they will always be in content
    user_id = content["user"]
    desired_order = content["desired_order"]
    # get method is used because depending on how frontend is created it may not allows be in content
    search_phrase = content.get("search")
    # the 3 priorities and 3 statuses represent the 3 instances of both that could form a number of combinations
    priority1 = content.get("priority1")
    priority2 = content.get("priority2")
    priority3 = content.get("priority3")
    status1 = content.get("status1")
    status2 = content.get("status2")
    status3 = content.get("status3")
    due_date = content.get("due_date")
    completed_date = content.get("completed_date")

    return user_id, desired_order, search_phrase, priority1, priority2, priority3, status1, status2, status3, due_date, completed_date



# orders the queryset depending on the user's desire
# originally had it grouped with filter_by_user but thought it would be computationally more efficient to be placed on
# smallest (last) queryset, additionally it makes the code more readable
def order_by_desired_order(queryset, desired_order):
    tasks = queryset.order_by(f"{desired_order}")
    return tasks

# filter for tasks with due dates that are today or latter using __gte (greater than or equal to)
def filter_by_upcoming(queryset):
    today = date.today()
    tasks = queryset.filter(due_date__gte=today)
    return tasks

# filters tasks by attempting to find tasks with the search_phrase in their title or description
def filter_by_search(queryset, search_phrase):
    # icontains was used because I dislike when I go to search for something and it is case sensitive 
    tasks = queryset.filter(Q(description__icontains=search_phrase) | Q(title__icontains=search_phrase))
    return tasks

# filters task by due_date
def filter_by_due_date(queryset, due_date):
    tasks = queryset.filter(due_date=due_date)
    return tasks

def filter_by_completed_date(queryset, completed_date):
    tasks = queryset.filter(completed_date=completed_date)
    return tasks

# filters tasks by selected priority either 1 or two priorities are selected
# priority1, 2 and 3 are either empty strings (in the case the user did not select them), or are the
# of the corresponding priority by matching the priority's id with the id that the user sends
def filter_by_priority(queryset, priority1, priority2, priority3):
    tasks = queryset.filter(Q(priority=priority1) | Q(priority=priority2) | Q(priority=priority3))
    return tasks

# filters task similarly to filter_by_priority
def filter_by_status(queryset, status1, status2, status3):
    tasks = queryset.filter(Q(status=status1) | Q(status=status2) | Q(status=status3))
    return tasks

# filters deleted tasks out of the queryset
def filter_by_deleted(queryset):
    tasks = queryset.filter(deleted=False)
    return tasks



# beginning of view functions

# returns a list of all uservos
@require_http_methods(["GET"])
def user_vo_list(request):
    if request.method == "GET":
        users = UserVO.objects.all()
        return JsonResponse(
            {"UserVOs": users},
            encoder=UserVOEncoder
        )

@require_http_methods(["GET"])
def status_list(request):
    status_list = Status.objects.all()
    return JsonResponse(
        {"Status List": status_list},
        encoder=StatusEncoder
    )

@require_http_methods(["GET"])
def priority_list(request):
    priority_list = Priority.objects.all()
    return JsonResponse(
        {"Priority_list": priority_list},
        encoder=PriorityEncoder
    )

# returns all tasks and allows a user to create a new task for themselves
@require_http_methods(["GET", "POST"])
def task_list(request):
    if request.method == "GET":
        tasks = Task.objects.all()
        not_deleted_tasks = filter_by_deleted(tasks)

        return JsonResponse(
            {"Tasks": not_deleted_tasks},
            encoder=TaskEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            # gets the user, status, priority ids from the frontend 
            user_id = content["user"]  
            status_id = content["status"]
            priority_id = content["priority"]
            
            # get the actually instances that will be used in the task creation
            priority_type = Priority.objects.get(id=priority_id)
            status_type = Status.objects.get(id=status_id)
            user = UserVO.objects.get(uservo_id=user_id)

            # delete the ids from the content so I can use kwargs to spread the data through on creation
            del content["user"], content["status"], content["priority"]

            # set the keys that Django expects to the values of their respective querysets
            content["user"] = user
            content["status"] = status_type
            content["priority"] = priority_type

            task = Task.objects.create(**content)
            return JsonResponse(
                {"Task": task},
                encoder=TaskEncoder
            )
        except Task.DoesNotExist:
            response = JsonResponse({"Error": "Was not able to create task"})
            response.status_code = 404
            return response

@require_http_methods(["GET"])
def task_detail(request, task_id):
    task = Task.objects.get(id=task_id)

    return JsonResponse(
        {"Task": task},
        encoder=TaskEncoder
    )


def all_users_tasks(request, desired_order, user_id):
    tasks = Task.objects.all()
    
    user_tasks = filter_by_user(tasks, user_id)
    not_deleted_tasks = filter_by_deleted(user_tasks)
    # order_by_desired_order always occurs last because sorting the queryset is the most computational complex
    # operation so it should be done on the smallest queryset possible
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def upcoming_user_tasks(request, desired_order, user_id):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    upcoming_tasks = filter_by_upcoming(user_tasks)
    not_deleted_tasks = filter_by_deleted(upcoming_tasks)
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def search_user_tasks(request, desired_order, user_id, search_phrase):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    not_deleted_tasks = filter_by_deleted(user_tasks)
    search_tasks = filter_by_search(not_deleted_tasks, search_phrase)
    ordered_tasks = order_by_desired_order(search_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def due_date_user_tasks(request, desired_order, user_id, due_date):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    date_tasks = filter_by_due_date(user_tasks, due_date)
    not_deleted_tasks = filter_by_deleted(date_tasks)
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def priority_user_tasks(request, desired_order, user_id, priority1, priority2, priority3):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    priority_tasks = filter_by_priority(user_tasks, priority1, priority2, priority3)
    not_deleted_tasks = filter_by_deleted(priority_tasks)
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def status_user_tasks(request, desired_order, user_id, status1, status2, status3):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    status_tasks = filter_by_status(user_tasks, status1, status2, status3)
    not_deleted_tasks = filter_by_deleted(status_tasks)
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)
    url = os.environ.get("CLOUDAMQP_URL_USER", "amqp://guest:guest@rabbitmq")

    

    return JsonResponse(
        {"Tasks": ordered_tasks, "url": url},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def upcoming_search_user_tasks(request, desired_order, user_id, search_phrase):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    upcoming_tasks = filter_by_upcoming(user_tasks)
    search_tasks = filter_by_search(upcoming_tasks, search_phrase)
    not_deleted_tasks = filter_by_deleted(search_tasks)
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def upcoming_priority_user_tasks(request, desired_order, user_id, priority1, priority2, priority3):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    upcoming_tasks = filter_by_upcoming(user_tasks)
    priority_tasks = filter_by_priority(upcoming_tasks, priority1, priority2, priority3)
    not_deleted_tasks = filter_by_deleted(priority_tasks)
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def upcoming_status_user_tasks(request, desired_order, user_id, status1, status2, status3):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    upcoming_tasks = filter_by_upcoming(user_tasks)
    status_tasks = filter_by_status(upcoming_tasks, status1, status2, status3)
    not_deleted_tasks = filter_by_deleted(status_tasks)
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
         encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def upcoming_status_priority_user_tasks(request, desired_order, user_id, status1, status2, status3, priority1, priority2, priority3):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    upcoming_tasks = filter_by_upcoming(user_tasks)
    status_tasks = filter_by_status(upcoming_tasks, status1, status2, status3)
    priority_tasks = filter_by_priority(status_tasks, priority1, priority2, priority3)
    not_deleted_tasks = filter_by_deleted(priority_tasks)
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def upcoming_search_priority_user_tasks(request, desired_order, user_id, search_phrase, priority1, priority2, priority3):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    upcoming_tasks = filter_by_upcoming(user_tasks)
    priority_tasks = filter_by_priority(upcoming_tasks, priority1, priority2, priority3)
    not_deleted_tasks = filter_by_deleted(priority_tasks)
    search_tasks = filter_by_search(not_deleted_tasks, search_phrase)
    ordered_tasks = order_by_desired_order(search_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def upcoming_search_status_user_tasks(request, desired_order, user_id, search_phrase, status1, status2, status3):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    upcoming_tasks = filter_by_upcoming(user_tasks)
    status_tasks = filter_by_status(upcoming_tasks, status1, status2, status3)
    not_deleted_tasks = filter_by_deleted(status_tasks)
    search_tasks = filter_by_search(not_deleted_tasks, search_phrase)
    ordered_tasks = order_by_desired_order(search_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def upcoming_search_status_priority_user_tasks(request, desired_order, user_id, search_phrase, status1, status2, status3, priority1, priority2, priority3):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    upcoming_tasks = filter_by_upcoming(user_tasks)
    status_tasks = filter_by_status(upcoming_tasks, status1, status2, status3)
    priority_tasks = filter_by_priority(status_tasks, priority1, priority2, priority3)
    not_deleted_tasks = filter_by_deleted(priority_tasks)
    search_tasks = filter_by_search(not_deleted_tasks, search_phrase)
    ordered_tasks = order_by_desired_order(search_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def search_due_date_user_tasks(request, desired_order, user_id, search_phrase, due_date):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    due_date_tasks = filter_by_due_date(user_tasks, due_date)
    not_deleted_tasks = filter_by_deleted(due_date_tasks)
    search_tasks = filter_by_search(not_deleted_tasks, search_phrase)
    ordered_tasks = order_by_desired_order(search_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def search_priority_user_tasks(request, desired_order, user_id, search_phrase, priority1, priority2, priority3):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    priority_tasks = filter_by_priority(user_tasks, priority1, priority2, priority3)
    not_deleted_tasks = filter_by_deleted(priority_tasks)
    search_tasks = filter_by_search(not_deleted_tasks, search_phrase)
    ordered_tasks = order_by_desired_order(search_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def search_status_user_tasks(request, desired_order, user_id, search_phrase, status1, status2, status3):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    status_tasks = filter_by_status(user_tasks, status1, status2, status3)
    not_deleted_tasks = filter_by_deleted(status_tasks)
    search_tasks = filter_by_search(not_deleted_tasks, search_phrase)
    ordered_tasks = order_by_desired_order(search_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def search_status_priority_user_tasks(request, desired_order, user_id, search_phrase, status1, status2, status3, priority1, priority2, priority3):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    status_tasks = filter_by_status(user_tasks, status1, status2, status3)
    priority_tasks = filter_by_priority(status_tasks, priority1, priority2, priority3)
    not_deleted_tasks = filter_by_deleted(priority_tasks)
    search_tasks = filter_by_search(not_deleted_tasks, search_phrase)
    ordered_tasks = order_by_desired_order(search_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder)

@require_http_methods(["GET"])
def search_due_date_priority_user_tasks(request, desired_order, user_id, search_phrase, priority1, priority2, priority3, due_date):
    tasks = Task.objects.all()
    
    user_tasks = filter_by_user(tasks, user_id)
    priority_tasks = filter_by_priority(user_tasks, priority1, priority2, priority3)
    due_date_tasks = filter_by_due_date(priority_tasks, due_date)
    not_deleted_tasks = filter_by_deleted(due_date_tasks)
    search_tasks = filter_by_search(not_deleted_tasks, search_phrase)
    ordered_tasks = order_by_desired_order(search_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def search_due_date_status_user_tasks(request, desired_order, user_id, search_phrase, status1, status2, status3, due_date):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    status_tasks = filter_by_status(user_tasks, status1, status2, status3)
    due_date_tasks = filter_by_due_date(status_tasks, due_date)
    not_deleted_tasks = filter_by_deleted(due_date_tasks)
    search_tasks = filter_by_search(not_deleted_tasks, search_phrase)
    ordered_tasks = order_by_desired_order(search_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def search_due_date_priority_status_user_tasks(request, desired_order, user_id, search_phrase, status1, status2, status3, priority1, priority2, priority3, due_date):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    status_tasks = filter_by_status(user_tasks, status1, status2, status3)
    priority_tasks = filter_by_priority(status_tasks, priority1, priority2, priority3)
    due_date_tasks = filter_by_due_date(priority_tasks, due_date)
    not_deleted_tasks = filter_by_deleted(due_date_tasks)
    search_tasks = filter_by_search(not_deleted_tasks, search_phrase)
    ordered_tasks = order_by_desired_order(search_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def due_date_status_user_tasks(request, desired_order, user_id, status1, status2, status3, due_date):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    status_tasks = filter_by_status(user_tasks, status1, status2, status3)
    due_date_tasks = filter_by_due_date(status_tasks, due_date)
    not_deleted_tasks = filter_by_deleted(due_date_tasks)
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def due_date_priority_user_tasks(request, desired_order, user_id, priority1, priority2, priority3, due_date):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    priority_tasks = filter_by_priority(user_tasks, priority1, priority2, priority3)
    due_date_tasks = filter_by_due_date(priority_tasks, due_date)
    not_deleted_tasks = filter_by_deleted(due_date_tasks)
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def due_date_priority_status_user_tasks(request, desired_order, user_id, status1, status2, status3, priority1, priority2, priority3, due_date):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    priority_tasks = filter_by_priority(user_tasks, priority1, priority2, priority3)
    status_tasks = filter_by_status(priority_tasks, status1, status2, status3)
    due_date_tasks = filter_by_due_date(status_tasks, due_date)
    not_deleted_tasks = filter_by_deleted(due_date_tasks)
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def priority_status_user_tasks(request, desired_order, user_id, status1, status2, status3, priority1, priority2, priority3):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    priority_tasks = filter_by_priority(user_tasks, priority1, priority2, priority3)
    status_tasks = filter_by_status(priority_tasks, status1, status2, status3)
    not_deleted_tasks = filter_by_deleted(status_tasks)
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def completed_date_user_tasks(request, desired_order, user_id, completed_date):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    completed_date_tasks = filter_by_completed_date(user_tasks, completed_date)
    not_deleted_tasks = filter_by_deleted(completed_date_tasks)
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def completed_date_priority_user_tasks(request, desired_order, user_id, priority1, priority2, priority3, completed_date):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    priority_tasks = filter_by_priority(user_tasks, priority1, priority2, priority3)
    completed_date_tasks = filter_by_completed_date(priority_tasks, completed_date)
    not_deleted_tasks = filter_by_deleted(completed_date_tasks)
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

# don't need a completed status view because all tasks with a completed_date will have the status of completed

@require_http_methods(["GET"])
def completed_date_search_user_tasks(request, desired_order, user_id, search_phrase, completed_date):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    completed_date_tasks = filter_by_completed_date(user_tasks, completed_date)
    not_deleted_tasks = filter_by_deleted(completed_date_tasks)
    search_tasks = filter_by_search(not_deleted_tasks, search_phrase)
    ordered_tasks = order_by_desired_order(search_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def completed_date_priority_status_user_tasks(request, desired_order, user_id, status1, status2, status3, priority1, priority2, priority3, completed_date):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    priority_tasks = filter_by_priority(user_tasks, priority1, priority2, priority3)
    status_tasks = filter_by_status(priority_tasks, status1, status2, status3)
    completed_date_tasks = filter_by_completed_date(status_tasks, completed_date)
    not_deleted_tasks = filter_by_deleted(completed_date_tasks)
    ordered_tasks = order_by_desired_order(not_deleted_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def completed_date_priority_search_user_tasks(request, desired_order, user_id, search_phrase, priority1, priority2, priority3, completed_date):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    completed_date_tasks = filter_by_completed_date(user_tasks, completed_date)
    priority_tasks = filter_by_priority(completed_date_tasks, priority1, priority2, priority3)
    not_deleted_tasks = filter_by_deleted(priority_tasks)
    search_tasks = filter_by_search(not_deleted_tasks, search_phrase)
    ordered_tasks = order_by_desired_order(search_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def completed_date_status_search_user_tasks(request, desired_order, user_id, search_phrase, status1, status2, status3, completed_date):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    completed_date_tasks = filter_by_completed_date(user_tasks, completed_date)
    status_tasks = filter_by_status(completed_date_tasks, status1, status2, status3)
    not_deleted_tasks = filter_by_deleted(status_tasks)
    search_tasks = filter_by_search(not_deleted_tasks, search_phrase)
    ordered_tasks = order_by_desired_order(search_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["GET"])
def completed_date_status_priority_search_user_tasks(request, desired_order, user_id, search_phrase, status1, status2, status3, priority1, priority2, priority3, completed_date):
    tasks = Task.objects.all()

    user_tasks = filter_by_user(tasks, user_id)
    completed_tasks = filter_by_completed_date(user_tasks, completed_date)
    status_tasks = filter_by_status(completed_tasks, status1, status2, status3)
    priority_tasks = filter_by_priority(status_tasks, priority1, priority2, priority3)
    not_deleted_tasks = filter_by_deleted(priority_tasks)
    search_tasks = filter_by_search(not_deleted_tasks, search_phrase)
    ordered_tasks = order_by_desired_order(search_tasks, desired_order)

    return JsonResponse(
        {"Tasks": ordered_tasks},
        encoder=TaskEncoder
    )

@require_http_methods(["PUT"])
def mark_task_complete(request, task_id):
    task = Task.objects.filter(id=task_id)

    # check if task is already completed
    if task[0].status.id == "3":
        return JsonResponse(
            {"Task": "Already complete"}
        )

    completed_status = Status.objects.get(id=3)
    today = date.today()

    uservo_id = task[0].user.uservo_id
    title = task[0].title

    task.update(status=completed_status, completed_date=today)
    send_points_data(uservo_id, title)
    

    return JsonResponse(
        {"Task": task},
        encoder=TaskEncoder
    )

# structured similar to the POST on task_list
@require_http_methods(["PUT"])
def update_task(request, task_id):
    try:
        content = json.loads(request.body)
        task = Task.objects.filter(id=task_id)
        
        # checks if task has been completed if it has it can not be updated or if it has been deleted(though the user should not be able to see deleted tasks)
        if task[0].status.status_type == "Completed" or task[0].deleted == True:
            return JsonResponse(
                {"Task": "Can not update the task"}
            )
        
        # gets the user, status, priority ids from the frontend 
        user_id = content["user"]
        status_id = content["status"]
        priority_id = content["priority"]
        
        # get the actually instances that will be used in the task creation
        priority_type = Priority.objects.get(id=priority_id)
        status_type = Status.objects.get(id=status_id)
        user = UserVO.objects.get(uservo_id=user_id)

        # delete the ids from the content so I can use kwargs to spread the data through on creation
        del content["user"], content["status"], content["priority"]

        # set the keys that Django expects to the values of their respective querysets
        content["user"] = user
        content["status"] = status_type
        content["priority"] = priority_type


        task.update(**content)
        return JsonResponse(
            {"Task": task},
            encoder=TaskEncoder
        )
    except Task.DoesNotExist:
        response = JsonResponse({"Task": "Was not able to update task"})
        response.status_code = 404
        return response

@require_http_methods(["DELETE"])
def delete_task(request, task_id):
    task = Task.objects.filter(id=task_id)
    task.update(deleted=True)

    return JsonResponse(
        {"Task": task},
        encoder=TaskEncoder
    )



