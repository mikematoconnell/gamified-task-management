import './App.css';

import { Routes, Route } from "react-router-dom"
import { useEffect, useState } from 'react';

import { UserContext } from './Utils/UserContext';

import { fetchUserInfo } from './Fetches/User Fetches/FetchUserInfo';
import { fetchUserFromJWT } from './Fetches/User Fetches/FetchUserFromJWT';

// Nav bar and React pages (pages are built from components)
import NavBar from './Components/NavBar/NavBar';
import LogIn from './Pages/Login/Login';
import Signup from './Pages/Signup/Singup';

import MyTasks from './Pages/My Tasks/MyTasks';
import TaskDetail from './Pages/TaskDetail/TaskDetail';
import UpdateTask from './Components/Update Task/UpdateTask';
import CreateTask from './Pages/CreateTask/CreateTask';
import Shop from './Pages/Shop/Shop';
import UnreadNotifications from './Pages/UnreadNotifications/UnreadNotifications';
import UserPage from './Pages/UserPage/UserPage';
import LeaderBoard from './Pages/LeaderBoardPage/LeaderBoard';

function App() {
  // this value is set in the login function the utils authorization file but kept here
  // so the whole project has access to the user information using the username on the JWT
  // and a user detail information fetch in the useEffect below
  const [userJWTInfo, setUserJWTInfo] = useState(null)
  const [userDetail, setUserDetail] = useState(null)
  const username = userJWTInfo?.username
  const [update, setUpdate] = useState(false)

  // this useEffect runs anytime the username or update is altered, username will be altered when a 
  // user signs in and the userJWTInfo will be used to fetch the user details
  useEffect(()=>{ 
    async function getUserInfo() {
      let payload = await fetchUserFromJWT()
      setUserJWTInfo(payload)
      let res = await fetch(`${process.env.REACT_APP_TASKS}tasks/uservo_list/`)
      let b = await res.json()
      console.log(b)
      // if the jwt_access_token is not available then username will be undefined, 
      // we don't want to make unnecessary fetches therefore the user detail will be set to undefined
      if (username !== undefined) {
        let detailPayload = await fetchUserInfo(username)
        setUserDetail(detailPayload)
      } else {
        setUserDetail(undefined)
      }

      setUpdate(false)
    }  
    getUserInfo()
  }, [username, update,])


  return (
    <UserContext.Provider value={{
      userJWTInfo, setUserJWTInfo, userDetail, setUserDetail, setUpdate, update
    }}>
      <Routes>
        <Route path="/" element={<NavBar setUpdate={setUpdate} update={update} />} >
          <Route index element={<MyTasks setUpdate={setUpdate} update={update} />} />
          <Route path="login/" element={<LogIn />} />
          <Route path="signup/" element={<Signup />} />
          <Route path="leaderboard/" element={<LeaderBoard />} />
          <Route path="taskdetail/" element={<TaskDetail />} />
          <Route path="taskdetail/update/" element={<UpdateTask setUpdate={setUpdate} update={update} />} />
          <Route path="createtask/" element={<CreateTask setUpdate={setUpdate} update={update} />} />
          <Route path="shop/" element={<Shop />} />
          <Route path="notifications/" element={<UnreadNotifications update={update} setUpdate={setUpdate} />} />
          <Route path="user/:username/" element={<UserPage />} />
        </Route>
      </Routes>
    </UserContext.Provider>
  );
}


export default App;
