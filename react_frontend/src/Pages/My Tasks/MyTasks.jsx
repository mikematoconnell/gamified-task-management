import "./MyTasks.css"

import { useContext, useState, useEffect } from "react"
import { UserContext } from "../../Utils/UserContext"

import TaskFilter from "../../Components/Filter/Filter"
import MyTasksTable from "../../Components/MyTasksTable/MyTasksTable"
import MobileTasks from "../../Components/MobileTasks/MobileTasks"

import { determineDesiredOrder } from "../../Utils/DetermineDesiredOrder"
import { createTaskUrl } from "../../Utils/CreateTaskUrl"

import { fetchTaskList } from "../../Fetches/Task List Fetches/FetchTaskList"
import { fetchMarkTaskComplete } from "../../Fetches/Task List Fetches/MarkComplete"
import { fetchDeleteTask } from "../../Fetches/Task List Fetches/FetchDeleteTask"


const MyTasks = ({setUpdate, update}) => {
    const [displayType, setDisplayType] = useState("none")



    // initialize these values here where they are needed to create the task fetch url
    // prop drilled to appropriate components
    const { userDetail } = useContext(UserContext)
    const userId = userDetail?.User?.id
    const [tasks, setTasks] = useState([])
    const [desiredOrder, setDesiredOrder] = useState("due_date")
    const [search, setSearch] = useState("")
    const [priority1, setPriority1] = useState(false)
    const [priority2, setPriority2] = useState(false)
    const [priority3, setPriority3] = useState(false)
    const [status1, setStatus1] = useState(false)
    const [status2, setStatus2] = useState(false)
    const [status3, setStatus3] = useState(false)
    const [upcoming, setUpcoming] = useState(true)
    const [dueDate, setDueDate] = useState("yyyy-MM-dd")
    const [completedDate, setCompletedDate] = useState("yyyy-MM-dd")

    // determines whether in mobile or desktop view based on height and width of computer
    const [mobile, setMobile] = useState(false)

    // this useEffect sets the mobile variable 
    useEffect(() => {
        function handleResize() {
            const width = window.innerWidth
            const height = window.innerHeight
            // condition on which the mobile variable is set
            if (height < 640 || width < 991){
                setMobile(true)
            } else {
                setMobile(false)
            }
        }
        window.addEventListener("resize", handleResize)
        
        handleResize()
        
        return () => { 
            window.removeEventListener("resize", handleResize)
        }
      }, [setMobile])
    
    
    useEffect(() => {
        async function getTasks() {
            // stops the fetch call from happening until enough data is loaded to make 
            if (userId !== undefined && desiredOrder !== undefined) {
                const taskUrl = createTaskUrl(
                    search, userId, 
                    priority1, priority2, 
                    priority3, status1, 
                    status2, status3, 
                    dueDate, completedDate, 
                    upcoming, desiredOrder)

                const taskList = await fetchTaskList(taskUrl)
                setTasks(taskList)
            }
        } 
        getTasks()
    },[desiredOrder, search, priority1, priority2, priority3, status1, status2, status3, dueDate, completedDate, userId, upcoming, update])

    // can only filter by one of upcoming/due date/completed date this handles that
    const handleDueDateUpcomingCompletedDate = (type, value) => {
        if (type === "dueDate") {
            setDueDate(value)
            setUpcoming(false)
            setCompletedDate("yyyy-MM-dd")
        } else if (type === "upcoming") {
            setDueDate("yyyy-MM-dd")
            setUpcoming(!upcoming)
            setCompletedDate("yyyy-MM-dd")
        // need to set statuses to false because all will be complete no need to filter by completed data and status
        } else if (type === "completedDate") {
            setStatus1(false)
            setStatus2(false)
            setStatus3(false)
            setDueDate("yyyy-MM-dd")
            setUpcoming(false)
            setCompletedDate(value)
        }else if (type==="priority") {
            setCompletedDate("yyyy-MM-dd")
        } else if (type === "clear") {
            setDueDate(null)
            setUpcoming(false)
            setCompletedDate(null)
        }
    }

    const handleDueDate = (event) => {
        const value = event.target.value
        const stringDate = value.toString()
        handleDueDateUpcomingCompletedDate("dueDate", stringDate)
    }

    const handleCompletedDate = (event) => {
        const value = event.target.value
        const stringDate = value.toString()
        handleDueDateUpcomingCompletedDate("completedDate", stringDate)
    }

    const handleUpcoming = () => {
        handleDueDateUpcomingCompletedDate("upcoming", null)
    }

    const handleStatus1 = () => {
        handleDueDateUpcomingCompletedDate("priority", null)
        setStatus1(!status1)
    }

    const handleStatus2 = () => {
        handleDueDateUpcomingCompletedDate("priority", null)
        setStatus2(!status2)
    }

    const handleStatus3 = () => {
        handleDueDateUpcomingCompletedDate("priority", null)
        setStatus3(!status3)
    }

    const handlePriority1 = () => {
        setPriority1(!priority1)
    }

    const handlePriority2 = () => {
        setPriority2(!priority2)
    }

    const handlePriority3 = () => {
        setPriority3(!priority3)
    }

    const handleFilterDisplay = () => {
        if (displayType === "none") {
            setDisplayType("block")
        } else if (displayType === "block") {
            setDisplayType("none")
        }
    }

    const handleSearch = (event) => {
        setSearch(event.target.value)
    }

    const handleDesiredOrder = (desiredOrderInput) => {
        const updatedDesiredOrder = determineDesiredOrder(desiredOrder, desiredOrderInput)

        setDesiredOrder(updatedDesiredOrder)
    }

    const handleMarkTaskComplete = async (taskId) => {
        const completeResponse = await fetchMarkTaskComplete(taskId)
        if (completeResponse.ok) {
            setUpdate(!update)
        }
    }

    const handleDeleteTask = async (taskId) => {
        const deleteResponse = await fetchDeleteTask(taskId)
        if (deleteResponse.ok) {
            setUpdate(!update) 
        } 
    }

    return (
        <div className="my-tasks-container">
            <div className="button-container">
                <button className="filter-button" onClick={() => handleFilterDisplay()}>Filter</button>
            </div>
            <TaskFilter style={{ display: displayType }} 
                handleSearch={handleSearch}  
                handleDueDate={handleDueDate}
                handleCompletedDate={handleCompletedDate} 
                handleUpcoming={handleUpcoming}
                upcoming={upcoming} 
                dueDate={dueDate}
                completedDate={completedDate}
                handleStatus1={handleStatus1}
                handleStatus2={handleStatus2}
                handleStatus3={handleStatus3}
                status1={status1}
                status2={status2}
                status3={status3}
                handlePriority1={handlePriority1}
                handlePriority2={handlePriority2}
                handlePriority3={handlePriority3}
                priority1={priority1}
                priority2={priority2}
                priority3={priority3}
                />
            
             {/* set up a mobile version and a desktop version because I wanted to use a table for the desktop
             but a repeating container for mobile and I didn't want both to load and one be hidden */}
            <MyTasksTable handleDesiredOrder={handleDesiredOrder} 
                handleDeleteTask={handleDeleteTask} 
                handleMarkTaskComplete={handleMarkTaskComplete} 
                tasks={tasks} 
                mobile={mobile} 
                setUpdate={setUpdate}
                update={update}
                />
            <MobileTasks  mobile={mobile}
                tasks={tasks} 

                />
      
            
        </div>
    )
}

export default MyTasks