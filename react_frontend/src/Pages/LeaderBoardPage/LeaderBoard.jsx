import "./LeaderBoard.css"
import { useEffect, useState } from "react"

import { fetchLeaderBoard } from "../../Fetches/Leader Board Fetches/FetchLeaderBoard"


const LeaderBoard = () => {
    const [leaderBoardData, setLeaderBoardData] = useState([])

    useEffect(() => {
        async function getLeaderBoardInfo() {
            let leaderBoardResponse = await fetchLeaderBoard()
            setLeaderBoardData(leaderBoardResponse)
        }
        getLeaderBoardInfo()
    },[])

    return (
        <div className="padding">

        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Rank</th>
                    <th>Username</th>
                    <th>Life Time Points</th>
                </tr>
            </thead>
            <tbody>
                {leaderBoardData.map((user, index) => {
                    return (
                    <tr key={user.id}>
                        <td>{index + 1}</td>
                        <td>{user.username}</td>
                        <td>{user.lifetime_points}</td>
                    </tr>
                    )
                })
                }
            </tbody>
        </table>
        </div>
    )
}

export default LeaderBoard