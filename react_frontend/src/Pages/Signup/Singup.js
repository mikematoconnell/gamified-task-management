import React, { useState } from 'react'
import { useAuthorization } from '../../Utils/Authorization'


export default function Signup() {
    const [userData, setUserData] = useState({
        username: "",
        password: "",
        first_name: "",
        last_name: "",
        email: "",
        city: "",
        state: "",
    })

    const [signupTest, setSignupTest] = useState(true)
    const { username, password, first_name, last_name, email } = userData
    const [, , signup] = useAuthorization()

    // List of US states for sign up form

    const changeHandler = e => {
        setUserData({ ...userData, [e.target.name]: [e.target.value] });
    }
    
    const submitHandler = e => {
        e.preventDefault()

        async function signupWithTest() {
            let test = await signup(
                userData.username[0],
                userData.password[0],
                userData.email[0],
                userData.first_name[0],
                userData.last_name[0],
            )

            if (test === false) {
                setSignupTest(false)
            } else {
                setSignupTest(true)
            }
        }
        signupWithTest()
    }

    let SignupFailed = function Waiting() {
        return (
            <div>
            </div>
        )
    }

    if (signupTest === false) {
        SignupFailed = function Failed() {
            return (
                <div className="alert alert-danger" role="alert">
                    Username Is taken. Please try again.
                </div>
            )
        }
    }

    return (
        <div className="d-flex text-center admin-bg ">
                <div className='card shadow login-card'>
                <div className="row gx-5">
                    <div className="col">
                        <div className="card shadow">
                            <div className="card px-4 py-4 body">
                                <div className='m-3'>
                                    <h1 align="center">Sign Up</h1>
                                </div>
                                <form onSubmit={submitHandler}>
                                    <div className='bt-3'>
                                        <SignupFailed />
                                        <div className="form-floating mb-2">
                                            <div className="row g-2">
                                                <div className="col md">
                                                    <div className='form-floating mb-1'>
                                                        <input className="form-control"
                                                            type="text"
                                                            name="first_name"
                                                            value={first_name}
                                                            onChange={changeHandler}
                                                            placeholder="First Name"
                                                            required
                                                        />
                                                        <label htmlFor="floatingInputGrid">First Name</label>
                                                    </div>
                                                </div>
                                                <div className="col md">
                                                    <div className='form-floating mb-1'>
                                                        <input className="form-control"
                                                            type="text"
                                                            name="last_name"
                                                            value={last_name}
                                                            onChange={changeHandler}
                                                            placeholder="Last Name"
                                                            required
                                                        />
                                                        <label htmlFor="floatingInputGrid">Last Name</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-floating mb-2">
                                        <input className="form-control"
                                            type="text"
                                            value={username}
                                            name="username"
                                            onChange={changeHandler}
                                            placeholder="Enter Username"
                                            required
                                        />
                                        <label htmlFor="floatingInputGrid">Username</label>
                                    </div>
                                    <div className="form-floating mb-2">
                                        <input className="form-control"
                                            type="password"
                                            value={password}
                                            name="password"
                                            onChange={changeHandler}
                                            placeholder="Enter Password"
                                            required
                                        />
                                        <label htmlFor="floatingInputGrid">Password</label>
                                    </div>
                                    <div className="form-floating mb-2">
                                        <input className="form-control"
                                            type="email"
                                            name="email"
                                            value={email}
                                            onChange={changeHandler}
                                            placeholder="Enter Email"
                                            required
                                        />
                                        <label htmlFor="floatingInputGrid">Email address</label>
                                    </div>
                                    <div className='m-4'>
                                        <button
                                            type="submit"
                                            className="btn btn-dark btn-lg  rounded-pill">Sign Up
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    );
}
