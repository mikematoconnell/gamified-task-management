import "./UserPage.css"

import { useEffect, useState } from "react"
import { useParams } from "react-router"

import { fetchUserInfo } from "../../Fetches/User Fetches/FetchUserInfo"

const UserPage = () => {
    const {username} = useParams()
    const [rewards, setRewards] = useState([])
    const [displayUsername, setDisplayUsername] = useState("")
    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [spendablePoints, setSpendablePoints] = useState(0)
    const [lifetimePoints, setLifetimePoints] = useState(0)

    useEffect(() => {
        async function getUserData() {
            if (username !== undefined) {
                const response = await fetchUserInfo(username)
                setRewards(response.User.rewards)
                setDisplayUsername(response.User.username)
                setLifetimePoints(response.User.lifetime_points)
                setSpendablePoints(response.User.spendable_points)
                setFirstName(response.User.first_name)
                setLastName(response.User.last_name)
            }
        }
        getUserData()
    }, [username])

    return (
        <div className="user-page-container">
        <div className="user-information">
            <h4 className="title-user-rewards">User Information:</h4>
            <p>Username: {displayUsername}</p>
            <p>First Name: {firstName}</p>
            <p>Last Name: {lastName}</p>
            <p>Spendable Points: {spendablePoints}</p>
            <p>Lifetime Points: {lifetimePoints}</p>
        </div> 
        <div className="rewards-container">
            <h3>My Rewards:</h3>
            <div className="inner-container">
                {rewards.map((reward) => {
                    return (
                        <div key={reward.pk} className="reward-container">
                            <img src={reward.picture} alt={reward.name} className="reward-image-container"></img>
                            <h4>{reward.name}</h4>
                        </div>
                    )
                })}
            </div>
        </div>
        </div>
    )
}

export default UserPage