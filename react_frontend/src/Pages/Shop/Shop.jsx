import "./Shop.css"

import { useContext, useEffect, useState } from "react"

import { UserContext } from "../../Utils/UserContext"

import { purchaseRewardFetch } from "../../Fetches/PurchaseRewardFetch/PurchaseRewardFetch"


const Shop = () => {
    const [rewardList, setRewardList] = useState([])
    const userDetail = useContext(UserContext)

    const update = userDetail.update
    const setUpdate = userDetail.setUpdate

    const spendablePoints = userDetail?.userDetail?.User?.spendable_points
    const userId = userDetail?.userDetail?.User?.id


    useEffect(() => {
        async function getRewards() {
            const config = {
                method: "get"
            }
            const response = await fetch(`${process.env.REACT_APP_USERS}users/reward_list/`, config)
            const rewardResponse = await response.json()
            const rewards = rewardResponse["Rewards"]
            setRewardList(rewards)

        }
        getRewards()
    }, [])

    const handlePurchase = async (rewardPk, rewardCost) => {
        const response = await purchaseRewardFetch(userId, rewardPk, rewardCost)
        if (response.Cost) {
            alert(response.Cost)
        } else {
            setUpdate(!update)
        }
    }

    return (
        <div>
        <h2>Rewards Shop</h2>
        <h4>User Points: {spendablePoints}</h4>
            <div className="rewards-container">
                {rewardList.map((reward) => {
                    return (
                        <div key={reward.pk} className="reward-container">
                            <img src={reward.picture} alt={reward.name} className="reward-image-container"></img>
                            <h4>{reward.name}</h4>
                            <p>Points Cost: {reward.required_points}</p>
                            <button className="reward-button" onClick={() => handlePurchase(reward.pk, reward.required_points)}>Purchase</button>
                        </div>
                    )
                })}
            </div>
        </div>
    )

    
}

export default Shop