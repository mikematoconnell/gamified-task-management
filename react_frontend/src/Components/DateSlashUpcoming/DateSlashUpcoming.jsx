import "./DateSlashUpcoming.css"

const DateSlashUpcoming = ({handleDueDate, handleCompletedDate, handleUpcoming, upcoming, dueDate, completedDate}) => {
    console.log(dueDate)
    return (
        <div className="date-upcoming-container">
            <h4>Filter by Date:</h4>
            <div className="date-upcoming-due-date">
                <label>Filter by Due Date:</label>
                <input type="date" value={dueDate} onChange={handleDueDate} />
            </div>
            <div className="date-upcoming-completed-date">

                <label>Filter by Completed Date:</label>
                <input type="date" value={completedDate} onChange={handleCompletedDate} />
            </div>
            <div className="date-upcoming-upcoming">
                <label className="checkbox-label">Filter by Upcoming:</label>
                <input className="checkbox-input" type="checkbox" checked={upcoming} onChange={handleUpcoming} />
            </div>
        </div>
    )
}

export default DateSlashUpcoming