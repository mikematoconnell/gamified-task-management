import "./StatusFilter.css"

const StatusFilter = ({handleStatus1, handleStatus2, handleStatus3, status1, status2, status3}) => {
    
    return (
        <div className="status-filter-container">
            <h4>Filter by Status:</h4>
            <div className="status-filter-to-do">
                <label className="checkbox-label">Filter by To-Do:</label>
                <input className="checkbox-input" type="checkbox" checked={status1} onChange={handleStatus1} />
            </div>
            <div className="status-filter-in-progress">
                <label className="checkbox-label">Filter by In-Progress:</label>
                <input className="checkbox-input" type="checkbox" checked={status2} onChange={handleStatus2} />
            </div>
            <div className="status-filter-in-progress">
                <label className="checkbox-label">Filter by Completed:</label>
                <input className="checkbox-input" type="checkbox" checked={status3} onChange={handleStatus3} />
            </div>
        </div>
    )
}

export default StatusFilter