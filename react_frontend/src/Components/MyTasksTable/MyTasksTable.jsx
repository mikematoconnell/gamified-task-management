import "./MyTasksTable.css"

import { useNavigate } from "react-router"


const MyTasksTable = ({handleDesiredOrder, handleDeleteTask, handleMarkTaskComplete, tasks, mobile}) => {
    const navigate = useNavigate()
    if (mobile) {
        return null
    }

    const handleNavigateTask = (taskId) => {
        navigate(`taskdetail/`, {state: {"taskId" :taskId}})
    }

    return (
        <table className="task-table">
            <thead>
                <tr>
                    <th onClick={() => handleDesiredOrder("title")} className="header-filter">Title</th>
                    <th onClick={() => handleDesiredOrder("description")} className="header-filter">Description</th>
                    <th onClick={() => handleDesiredOrder("status")} className="header-filter">Status</th>
                    <th onClick={() => handleDesiredOrder("priority")} className="header-filter">Priority</th>
                    <th onClick={() => handleDesiredOrder("due_date")} className="header-filter">Due Date</th>
                    <th>Mark Complete</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {tasks.map((task) => {
                    return (
                    <tr key={task.id} className="task-row">
                        <td onClick={() => handleNavigateTask(task.id)}>{task.title}</td>
                        <td onClick={() => handleNavigateTask(task.id)}>{task.description.slice(0, 20)}...</td>
                        <td onClick={() => handleNavigateTask(task.id)}>{task.status.status_type}</td>
                        <td onClick={() => handleNavigateTask(task.id)}>{task.priority.priority_type}</td>
                        <td onClick={() => handleNavigateTask(task.id)}>{task.due_date}</td>
                        
                        {task.status.id === 3 ? (
                            <td></td>
                        ) : (
                            <td><button className="btn btn-success" style={{"zIndex":"10"}} onClick={ () => handleMarkTaskComplete(task.id)}>Completed</button></td>
                        )}
                        <td><button className="btn btn-danger" onClick={ () => handleDeleteTask(task.id)}>Delete</button></td>
                    </tr>
                    )
                })
                }
            </tbody>
        </table>
    )
}

export default MyTasksTable