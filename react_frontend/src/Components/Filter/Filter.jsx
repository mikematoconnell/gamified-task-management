import "./Filter.css"

import DateSlashUpcoming from "../DateSlashUpcoming/DateSlashUpcoming"
import StatusFilter from "../Status Filter/StatusFilter"
import PriorityFilter from "../Priority Filter/PriorityFilter"
import SearchBar from "../../Components/SearchBar/SearchBar"


const TaskFilter = ({handleSearch, style, handleDueDate, handleCompletedDate, handleUpcoming, upcoming, dueDate, completedDate, handleStatus1, handleStatus2, handleStatus3, status1, status2, status3, handlePriority1, handlePriority2, handlePriority3, priority1, priority2, priority3}) => {
    return (
        <div style={style}>
        <div className="filter-container">
            <DateSlashUpcoming handleDueDate={handleDueDate} 
                handleCompletedDate={handleCompletedDate} 
                handleUpcoming={handleUpcoming}
                upcoming={upcoming} 
                dueDate={dueDate}
                completedDate={completedDate}
                />
            <StatusFilter handleStatus1={handleStatus1} 
                handleStatus2={handleStatus2}
                handleStatus3={handleStatus3}
                status1={status1}
                status2={status2}
                status3={status3} 
                />
            <PriorityFilter handlePriority1={handlePriority1}
                handlePriority2={handlePriority2}
                handlePriority3={handlePriority3}
                priority1={priority1}
                priority2={priority2}
                priority3={priority3}
                />
            <SearchBar handleSearchBar={handleSearch} />
        </div>
        </div>
    )
}

export default TaskFilter