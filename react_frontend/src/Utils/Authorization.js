import { useContext } from "react"
import { useNavigate } from "react-router-dom"

import {UserContext} from "./UserContext"

import { fetchUserFromJWT } from "../Fetches/User Fetches/FetchUserFromJWT"


export const useAuthorization = () => {
    // allows functions to navigate throughout the application
    const navigate = useNavigate()
    // pulls user date (userInfo) and a setter function for the user data which will be applied on successful login or signup
    const { userJWTInfo, setUserJWTInfo } = useContext(UserContext)

    const logout = async () => {
        // this checks if there is a signed in user
        if (userJWTInfo?.username) {
            // the logout url provided by djwto and the REACT_APP_USERS environment variable found create in the Docker Compose file
            const logoutUrl = `${process.env.REACT_APP_USERS}api/token/refresh/logout`
            const logoutConfig = {
                method: "delete",
                credentials: "include"
            }

            const response = await fetch(logoutUrl, logoutConfig)

            // checks if the response sends back a success if so it sets the userJWTInfo to null
            if (response.ok) {
                setUserJWTInfo(null)
                navigate("/login")
                return(response)
            }
        }
    }

    const login = async (username, password) => {
        // djwto's login function only accepts the username and password in form format therefore need to set up a new 
        // form with the username and password added to it
        const form = new FormData()
        form.append("username", username)
        form.append("password", password)
                
        const loginUrl = `${process.env.REACT_APP_USERS}login/`
        const loginConfig = {
            method: "post",
            credentials: "include",
            body: form
        }
        
        const response = await fetch(loginUrl, loginConfig)
        if (response.ok) {
            // calls the fetchUserTokenData, then sets the user
            let payload = await fetchUserFromJWT()
            setUserJWTInfo(payload)
            navigate("/")
        }
        // if an error occurs in the login response the response is returned and the user is notified
        return response
    }

    const signup = async (username, password, email, first_name, last_name) => {
        const signupContent = JSON.stringify({username, password, email, first_name, last_name})

        const signupUrl = `${process.env.REACT_APP_USERS}users/`
        const signupConfig = {
            method: "post",
            body: signupContent,
            headers: {
                "Content-Type": "application/json"
            },
        }
        
        const response = await fetch(signupUrl, signupConfig)
        
        // checks if the response is ok and if it is calls the login function above to sign the user in
        if (response.ok) {
            return await login(username, password)
        }
    }

    const update = async (username, password, email, first_name, last_name, notifications_on) => {
        const updateContent = JSON.stringify({
            username,
            first_name,
            last_name,
            password,
            notifications_on,
            email
        })
        
        const updateUrl = `${process.env.REACT_APP_USERS}api/accounts/`
        const updateConfig = {
            method: "post",
            body: updateContent,
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(updateUrl, updateConfig)

        if (response.ok) {
            return await login(username, password)
        }
    }

    return [logout, login, signup, update]
}