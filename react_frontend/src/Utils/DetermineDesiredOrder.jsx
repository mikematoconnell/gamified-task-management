export const determineDesiredOrder = (currentDesiredOrder, desiredOrderInput) => {
    let updatedDesiredOrder
    if (currentDesiredOrder === desiredOrderInput) {
        updatedDesiredOrder = "-" + desiredOrderInput
    } else if ("-" + currentDesiredOrder === desiredOrderInput) {
        updatedDesiredOrder = desiredOrderInput
    } else {
        updatedDesiredOrder = desiredOrderInput
    }

    return updatedDesiredOrder
}