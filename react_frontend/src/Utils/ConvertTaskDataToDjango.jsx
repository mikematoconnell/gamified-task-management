export const ConvertTaskDataToDjango = 
    (priority1, priority2,
    priority3, status1,
    status2, status3,
    ) => {
        // initializes the variables if they are initialized only inside of the if else statements it can cause issues
        // no value is given because they all will be given either a value of 1,2,3 or null depending on their values

        let updatedPriority1, updatedPriority2, updatedPriority3, updatedStatus1, updatedStatus2, updatedStatus3
        
        // this is transforming the true or false values that were used on the My Task page into the id values used in 
        // Django queryset look up functions
        if (priority1) {
            updatedPriority1 = 1
        } else {
            // if priority1, pirority2, priority3, status1, status2, or status3 are false they are set to 0
            // this is because on the backend variable urls do not allow for null values, 0 is safe to use
            // because no priority or status instance has an id of 0 so the queryset helper functions will 
            // only include ones set to a proper id value such as 1, 2, or 3
            updatedPriority1 = 0
        } 
        if (priority2) {
            updatedPriority2 = 2
        } else {
            updatedPriority2 = 0
        } 
        if (priority3) {
            updatedPriority3 = 3
        } else {
            updatedPriority3 = 0
        } 
        if (status1) {
            updatedStatus1 = 1
        } else {
            updatedStatus1 = 0
        } 
        if (status2) {
            updatedStatus2 = 2
        } else {
            updatedStatus2 = 0
        } 
        if (status3) {
            updatedStatus3 = 3
        } else {
            updatedStatus3 = 0
        }

        // are stringified in preparation for json transmission and assigned to their 
        // appropriate key value that is associated with the Django backend
   
        return [updatedStatus1, updatedStatus2, updatedStatus3, updatedPriority1, updatedPriority2, updatedPriority3]
}