import { ConvertTaskDataToDjango } from "./ConvertTaskDataToDjango"

export const createTaskUrl = 
    (search, userId,
    priority1, priority2,
    priority3, status1,
    status2, status3,
    dueDate, completedDate,
    upcoming, desiredOrder) => {
        
        const [updatedStatus1, updatedStatus2, updatedStatus3, updatedPriority1, updatedPriority2, updatedPriority3] = ConvertTaskDataToDjango(priority1, priority2, priority3, status1, status2, status3)
        
        // initializes the variables if they are initialized only inside of the if else statements it can cause issues
        // these filters will be used in if else statements to determine the appropriate url
        // by default set to false so else statements doe not have to be written resulting in easier to read code
        let searchFilter = false
        let priorityFilter = false
        let statusFilter = false
        let dueDateFilter = false
        let completedDateFilter = false
        let upcomingFilter = false

        let baseUrl = `${process.env.REACT_APP_TASKS}tasks/task_list/user/${desiredOrder}/${userId}/`

        // determining which filters need to be applied
        if (search.length > 0) {
            searchFilter = true
        } 
        if (priority1 || priority2 || priority3) {
            priorityFilter = true
        } 
        if (status1 || status2 || status3) {
            statusFilter = true
        } 

        if (dueDate !== "yyyy-MM-dd" && dueDate.length !== 0) {
            dueDateFilter = true
        } 
        if (completedDate !== "yyyy-MM-dd" && completedDate.length !== 0) {
            completedDateFilter = true
        }
        if (upcoming) {
            upcomingFilter = true
        }

        // determining based on which filters need to be applied which user task url to use
        if (!searchFilter && !priorityFilter && !statusFilter && !dueDateFilter && !completedDateFilter && !upcomingFilter) {
            return baseUrl + "all_tasks/"
        }
        if (searchFilter && !priorityFilter && !statusFilter && !dueDateFilter && !completedDateFilter && !upcomingFilter) {
            return baseUrl + `${search}/search_tasks/`
        }
        if (!searchFilter && !priorityFilter && !statusFilter && dueDateFilter && !completedDateFilter && !upcomingFilter) {
            return baseUrl + `${dueDate}/due_date_tasks/`
        }
        if (!searchFilter && !priorityFilter && !statusFilter && !dueDateFilter && completedDateFilter && !upcomingFilter) {
            return baseUrl + `${completedDate}/completed_tasks/`
        }
        if (!searchFilter && !priorityFilter && !statusFilter && !dueDateFilter && !completedDateFilter && upcomingFilter) {
            console.log(baseUrl)
            return baseUrl + `upcoming_tasks/`
        }
        if (!searchFilter && !priorityFilter && statusFilter && !dueDateFilter && !completedDateFilter && !upcomingFilter) {
            return baseUrl + `${updatedStatus1}/${updatedStatus2}/${updatedStatus3}/status_tasks/`
        }
        if (!searchFilter && priorityFilter && !statusFilter && !dueDateFilter && !completedDateFilter && !upcomingFilter) {
            return baseUrl + `${updatedPriority1}/${updatedPriority2}/${updatedPriority3}/priority_tasks/`
        }
        if (searchFilter && !priorityFilter && !statusFilter && !dueDateFilter && !completedDateFilter && upcomingFilter) {
            return baseUrl + `${search}/upcoming_search_tasks/`
        }
        if (!searchFilter && priorityFilter && !statusFilter && !dueDateFilter && !completedDateFilter && upcomingFilter) {
            return baseUrl + `${updatedPriority1}/${updatedPriority2}/${updatedPriority3}/upcoming_priority_tasks/`
        }
        if (!searchFilter && !priorityFilter && statusFilter && !dueDateFilter && !completedDateFilter && upcomingFilter) {
            return baseUrl + `${updatedStatus1}/${updatedStatus2}/${updatedStatus3}/upcoming_status_tasks/`
        }
        if (searchFilter && priorityFilter && !statusFilter && !dueDateFilter && !completedDateFilter && upcomingFilter) {
            return baseUrl + `${search}/${updatedPriority1}/${updatedPriority2}/${updatedPriority3}/upcoming_search_priority_tasks/`
        }
        if (searchFilter && !priorityFilter && statusFilter && !dueDateFilter && !completedDateFilter && upcomingFilter) {
            return baseUrl + `${search}/${updatedStatus1}/${updatedStatus2}/${updatedStatus3}/upcoming_search_status_tasks/`
        }
        if (searchFilter && priorityFilter && statusFilter && !dueDateFilter && !completedDateFilter && upcomingFilter) {
            return baseUrl + `${search}/${updatedStatus1}/${updatedStatus2}/${updatedStatus3}/${updatedPriority1}/${updatedPriority2}/${updatedPriority3}/upcoming_search_status_priority_tasks/`
        }
        if (searchFilter && !priorityFilter && !statusFilter && dueDateFilter && !completedDateFilter && !upcomingFilter) {
            return baseUrl + `${search}/${dueDate}/search_due_date_tasks/`
        }
        if (searchFilter && priorityFilter && !statusFilter && !dueDateFilter && !completedDateFilter && !upcomingFilter) {
            return baseUrl + `${search}/${updatedPriority1}/${updatedPriority2}/${updatedPriority3}/search_priority_tasks/`
        }
        if (searchFilter && !priorityFilter && statusFilter && !dueDateFilter && !completedDateFilter && !upcomingFilter)  {
            return baseUrl + `${search}/${updatedStatus1}/${updatedStatus2}/${updatedStatus3}/search_status_tasks/`
        }
        if (searchFilter && priorityFilter && statusFilter && !dueDateFilter && !completedDateFilter && !upcomingFilter) {
            return baseUrl + `${search}/${updatedStatus1}/${updatedStatus2}/${updatedStatus3}/${updatedPriority1}/${updatedPriority2}/${updatedPriority2}/search_status_priority_tasks/`
        }
        if (searchFilter && priorityFilter && !statusFilter && dueDateFilter && !completedDateFilter && !upcomingFilter) {
            return baseUrl + `${search}/${updatedPriority1}/${updatedPriority2}/${updatedPriority3}/${dueDate}/search_due_date_priority_tasks/`
        }
        if (searchFilter && !priorityFilter && statusFilter && dueDateFilter && !completedDateFilter && !upcomingFilter) {
            return baseUrl + `${search}/${updatedStatus1}/${updatedStatus2}/${updatedStatus3}/${dueDate}/search_due_date_status_tasks/`
        }
        if (!searchFilter && !priorityFilter && statusFilter && dueDateFilter && !completedDateFilter && !upcomingFilter) {
            return baseUrl + `${updatedStatus1}/${updatedStatus2}/${updatedStatus3}/${dueDate}/due_date_status_tasks/`
        }
        if (!searchFilter && priorityFilter && !statusFilter && dueDateFilter && !completedDateFilter && !upcomingFilter) {
            return baseUrl + `${updatedPriority1}/${updatedPriority2}/${updatedPriority3}/${dueDate}/due_date_priority_tasks/`
        }
        if (!searchFilter && priorityFilter && statusFilter && dueDateFilter && !completedDateFilter && !upcomingFilter) {
            return baseUrl + `${updatedStatus1}/${updatedStatus2}/${updatedStatus3}/${updatedPriority1}/${updatedPriority2}/${updatedPriority3}/${dueDate}/due_date_priority_status_tasks/`
        }
        if (!searchFilter && priorityFilter && statusFilter && !dueDateFilter && !completedDateFilter && !upcomingFilter) {
            return baseUrl + `${updatedStatus1}/${updatedStatus2}/${updatedStatus3}/${updatedPriority1}/${updatedPriority2}/${updatedPriority3}/priority_status_tasks/`
        }
        if (!searchFilter && priorityFilter && !statusFilter && !dueDateFilter && completedDateFilter && !upcomingFilter) {
            return baseUrl + `${updatedPriority1}/${updatedPriority2}/${updatedPriority3}/${completedDate}/completed_priority_tasks/`
        }
        if (searchFilter && !priorityFilter && !statusFilter && !dueDateFilter && completedDateFilter && !upcomingFilter) {
            return baseUrl + `${search}/${completedDate}/completed_search_tasks/`
        }
        if (searchFilter && priorityFilter && !statusFilter && !dueDateFilter && completedDateFilter && !upcomingFilter) {
            return baseUrl + `${search}/${updatedPriority1}/${updatedPriority2}/${updatedPriority3}/${completedDate}/completed_priority_search_tasks/`
        }
        if (searchFilter && priorityFilter && statusFilter && dueDateFilter && !completedDateFilter && !upcomingFilter) {
            return baseUrl + `${search}/${updatedStatus1}/${updatedStatus2}/${updatedStatus3}/${updatedPriority1}/${updatedPriority2}/${updatedPriority3}/${dueDate}/search_due_date_priority_status_tasks/`
        }
        if (!searchFilter && priorityFilter && statusFilter && !dueDateFilter && !completedDateFilter && upcomingFilter) {
            return baseUrl + `${updatedStatus1}/${updatedStatus2}/${updatedStatus3}/${updatedPriority1}/${updatedPriority2}/${updatedPriority3}/upcoming_status_priority_tasks/`
        }
    }