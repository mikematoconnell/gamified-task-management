export const fetchStatuses = async () => {
    const statusesUrl = `${process.env.REACT_APP_TASKS}tasks/status_list/`
    const statusesConfig = {
        method: "get"
    }
    const response = await fetch(statusesUrl, statusesConfig)
    const jsonResponse = await response.json()
    const statusList = jsonResponse["Status List"]
    return statusList
}