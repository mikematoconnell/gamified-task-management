export const purchaseRewardFetch = async (userId, rewardPk, rewardCost) => {
    const url = `${process.env.REACT_APP_USERS}users/purchase_reward/${userId}/${rewardPk}/`
    const config = {
        method: "put",
        body: JSON.stringify({"Cost": rewardCost}),
        headers: {
            "Content-Type": "application/json"
        }
    }
    const response = await fetch(url, config)
    const unJsonResponse = await response.json()
    return unJsonResponse
} 