export const fetchLeaderBoard = async () => {
    const leaderBoardUrl = `${process.env.REACT_APP_USERS}users/leader_board/`
    // unlike fetches that involve personal user data that are protected with jwt authorization
    // this fetch does not need the credentials: "include"
    const leaderBoardConfig = {
        method: "get"
    }
    const response = await fetch(leaderBoardUrl, leaderBoardConfig)
    const leaderBoardData = await response.json()
    // what is return is in the format {"Users": array of users}
    // in order to just get the array we use bracket notation to get the value associate with Users
    console.log(leaderBoardData)
    const usersLeaderBoardData = leaderBoardData["Users"]
    return usersLeaderBoardData
}