export const taskDetailFetch = async (taskId) => {
    const taskUrl = `${process.env.REACT_APP_TASKS}tasks/task_detail/${taskId}`
    const fetchConfigTask = {
        method: "get"
    }
    const response = await fetch(taskUrl, fetchConfigTask)
    const taskJson = await response.json()
    const taskObject = await taskJson["Task"]
    return taskObject
}