export const fetchUnreadNotificationCount = async (userId) => {
    const url = `${process.env.REACT_APP_USERS}users/notifications/count/${userId}/`
    const fetchConfig = {
        method: "get"
    }
    const response = await fetch(url, fetchConfig)
    const unJsonResponse = await response.json()
    const count = unJsonResponse["Count"]
    return count
}