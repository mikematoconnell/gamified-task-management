export const fetchDeleteTask = async  (taskId) => {
    const fetchConfig = {
        method: "delete"
    }

    const fetchUrl = `${process.env.REACT_APP_TASKS}tasks/delete_task/${taskId}/`
    const response = await fetch(fetchUrl, fetchConfig)
    return response
}