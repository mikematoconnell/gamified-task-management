export const fetchTaskList = async (taskUrl) => {
    const fetchConfigTasks = {
        method: "get"
    }

    const response = await fetch(taskUrl, fetchConfigTasks)
    const taskObject = await response.json()
    console.log(taskObject, "here")
    const taskList = taskObject["Tasks"]
    return taskList
}