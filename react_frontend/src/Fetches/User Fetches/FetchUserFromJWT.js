// this fetch request attempts to take parse the user's username and id from the jwt_access token and return it
export async function fetchUserFromJWT () {
    const payloadTokenUrl = `${process.env.REACT_APP_USERS}users/token/`
    const fetchConfigToken = {
          method: "get",
          credentials: "include"
        }
    const tokenResponse = await fetch(payloadTokenUrl, fetchConfigToken)
    const tokenReturned = await tokenResponse.json()
    const payload = tokenReturned["username"]
    return payload
}